package com.antoonvereecken.springstatemachine.config;

import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.service.PaymentServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.guard.Guard;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

import java.util.EnumSet;
import java.util.Random;

@Slf4j
@RequiredArgsConstructor
@EnableStateMachineFactory
@Configuration
public class StateMachineConfig extends StateMachineConfigurerAdapter<PaymentState, PaymentEvent> {

    /* MAKE SURE PROP-NAME = CLASS !!! (not configuring constructor, just use Lombok :) */
    private final Action<PaymentState, PaymentEvent> preAuthAction; // ../action/PreAuthAction
    private final Action<PaymentState, PaymentEvent> authAction; // ../action/AuthAction
    private final Guard<PaymentState, PaymentEvent> paymentIdGuard; // ../guard/PaymentIdGuard
    private final Action<PaymentState, PaymentEvent> preAuthApprovedAction; // ../action/PreAuthApprovedAction
    private final Action<PaymentState, PaymentEvent> preAuthDeclinedAction; // ../action/PreAuthDeclinedAction
    private final Action<PaymentState, PaymentEvent> authApprovedAction; // ../action/AuthApprovedAction
    private final Action<PaymentState, PaymentEvent> authDeclinedAction; // ../action/AuthDeclinedAction

    /* config states */
    @Override
    public void configure(StateMachineStateConfigurer<PaymentState, PaymentEvent> states) throws Exception {
        states.withStates()
                // INITIAL STATE
                .initial(PaymentState.NEW)
                // USED STATES
                .states(EnumSet.allOf(PaymentState.class))
                // TERMINAL STATES
                .end(PaymentState.AUTH) // SUCCESS
                .end(PaymentState.PRE_AUTH_ERROR) // ERROR
                .end(PaymentState.AUTH_ERROR) // ERROR
        ;
    }

    /* config transitions through states */
    @Override
    public void configure(StateMachineTransitionConfigurer<PaymentState, PaymentEvent> transitions) throws Exception {
        transitions.withExternal()
                .source(PaymentState.NEW).target(PaymentState.NEW)
                .event(PaymentEvent.PRE_AUTHORIZE)
                .action(preAuthAction)
                .guard(paymentIdGuard)

            .and().withExternal()
                .source(PaymentState.NEW).target(PaymentState.PRE_AUTH)
                .event(PaymentEvent.PRE_AUTH_APPROVED)
                .action(preAuthApprovedAction)

            .and().withExternal()
                .source(PaymentState.NEW).target(PaymentState.PRE_AUTH_ERROR)
                .event(PaymentEvent.PRE_AUTH_DECLINED)
                .action(preAuthDeclinedAction)

            .and().withExternal()
                .source(PaymentState.PRE_AUTH).target(PaymentState.AUTH_ERROR)
                .event(PaymentEvent.PRE_AUTH_DECLINED)
                .action(authDeclinedAction)

            .and().withExternal()
                .source(PaymentState.PRE_AUTH).target(PaymentState.PRE_AUTH)
                .event(PaymentEvent.AUTHORIZE)
                .action(authAction)

            .and().withExternal()
                .source(PaymentState.PRE_AUTH).target(PaymentState.AUTH)
                .event(PaymentEvent.AUTH_APPROVED)
                .action(authApprovedAction)
        ;
    }

    /* configure with listener */
    @Override
    public void configure(StateMachineConfigurationConfigurer<PaymentState, PaymentEvent> config) throws Exception {
        StateMachineListenerAdapter<PaymentState, PaymentEvent> smListenerAdapter = new StateMachineListenerAdapter<>(){
            @Override
            public void stateChanged(State<PaymentState, PaymentEvent> from, State<PaymentState, PaymentEvent> to) {
                System.out.println("stateChanged(): " + from + " --> " + to);
            }
        };
        config.withConfiguration().listener(smListenerAdapter);
    }

}
