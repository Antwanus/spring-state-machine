package com.antoonvereecken.springstatemachine.config.guard;

import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.service.PaymentServiceImpl;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

@Component
public class PaymentIdGuard implements Guard<PaymentState, PaymentEvent> {

    /** ? evaluate(StateContext<S,E>)
     *
     * returns true if the header has a PAYMENT_ID prop in the header
     * else false
     */
    @Override
    public boolean evaluate(StateContext<PaymentState, PaymentEvent> stateContext) {
        return stateContext.getMessageHeader(PaymentServiceImpl.PAYMENT_ID_HEADER) != null;
    }

}
