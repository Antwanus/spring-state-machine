package com.antoonvereecken.springstatemachine.config.action;

import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.service.PaymentServiceImpl;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class PreAuthAction implements Action<PaymentState, PaymentEvent> {

    @Override
    public void execute(StateContext<PaymentState, PaymentEvent> stateContext) {
        System.out.println("PreAuthAction: *** EXECUTING PRE-AUTH ACTION ***");

        if(new Random().nextInt(10) < 8){
            System.out.println("+++__PRE-AUTH APPROVED__+++");
            stateContext.getStateMachine()
                    .sendEvent(MessageBuilder.withPayload(PaymentEvent.PRE_AUTH_APPROVED)
                    .setHeader(
                            PaymentServiceImpl.PAYMENT_ID_HEADER,
                            stateContext.getMessageHeader(PaymentServiceImpl.PAYMENT_ID_HEADER)
                    )
                    .build());
        } else {
            System.out.println("xxwxxREFUSEDxxxxx");
            stateContext.getStateMachine()
                    .sendEvent(MessageBuilder.withPayload(PaymentEvent.PRE_AUTH_DECLINED)
                    .setHeader(
                            PaymentServiceImpl.PAYMENT_ID_HEADER,
                            stateContext.getMessageHeader(PaymentServiceImpl.PAYMENT_ID_HEADER)
                    )
                    .build());
        }

    }
}
