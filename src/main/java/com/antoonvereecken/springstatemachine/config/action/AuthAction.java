package com.antoonvereecken.springstatemachine.config.action;

import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.service.PaymentServiceImpl;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AuthAction implements Action<PaymentState, PaymentEvent> {

    @Override
    public void execute(StateContext<PaymentState, PaymentEvent> stateContext) {
        System.out.println("AuthAction: *** EXECUTING AUTH ACTION ***");

        if (new Random().nextInt(10) < 5) {
            System.out.println("AUTHORIZATION SUCCESS");
            stateContext.getStateMachine()
                    .sendEvent(MessageBuilder.withPayload(PaymentEvent.AUTH_APPROVED)
                            .setHeader(
                                    PaymentServiceImpl.PAYMENT_ID_HEADER,
                                    stateContext.getMessageHeader(PaymentServiceImpl.PAYMENT_ID_HEADER)
                            )
                            .build());
        } else {
            System.out.println("...U DON'T GOT NO MONEY, TAKE YO BROKE ASS HOME");
            stateContext.getStateMachine()
                    .sendEvent(MessageBuilder.withPayload(PaymentEvent.PRE_AUTH_DECLINED)
                            .setHeader(
                                    PaymentServiceImpl.PAYMENT_ID_HEADER,
                                    stateContext.getMessageHeader(PaymentServiceImpl.PAYMENT_ID_HEADER)
                            )
                            .build());
        }
    }
}
