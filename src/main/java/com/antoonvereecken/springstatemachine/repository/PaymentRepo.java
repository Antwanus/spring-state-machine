package com.antoonvereecken.springstatemachine.repository;

import com.antoonvereecken.springstatemachine.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepo extends JpaRepository<Payment, Long> {

}
