package com.antoonvereecken.springstatemachine.service;

import com.antoonvereecken.springstatemachine.domain.Payment;
import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.repository.PaymentRepo;
import com.antoonvereecken.springstatemachine.service.interceptor.PaymentStateChangeInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class PaymentServiceImpl implements PaymentService {

    public static final String PAYMENT_ID_HEADER = "payment_id";

    private final PaymentRepo repo;
    private final StateMachineFactory<PaymentState, PaymentEvent> smFactory;
    private final PaymentStateChangeInterceptor paymentStateChangeInterceptor;


    @Transactional
    @Override
    public Payment newPayment(Payment payment) {
        payment.setState(PaymentState.NEW);
        return repo.save(payment);
    }

    @Transactional
    @Override
    public StateMachine<PaymentState, PaymentEvent> preAuthorizePayment(Long paymentId) {
        StateMachine<PaymentState, PaymentEvent> sm = build(paymentId);
        sendEvent(paymentId, sm, PaymentEvent.PRE_AUTHORIZE);
        return sm;
    }

    @Transactional
    @Override
    public StateMachine<PaymentState, PaymentEvent> authorizePayment(Long paymentId) {
        StateMachine<PaymentState, PaymentEvent> sm = build(paymentId);
        sendEvent(paymentId, sm, PaymentEvent.AUTHORIZE);

        return sm;
    }

    @Deprecated // not needed using actions to progess
    @Transactional
    @Override
    public StateMachine<PaymentState, PaymentEvent> declinePayment(Long paymentId) {
        StateMachine<PaymentState, PaymentEvent> sm = build(paymentId);
        sendEvent(paymentId, sm, PaymentEvent.AUTH_DECLINED);
        return sm;
    }


    /* send event (change state via paymentId) to the statemachine */
    private void sendEvent(Long paymentId, StateMachine<PaymentState, PaymentEvent> sm, PaymentEvent event) {
        Message<PaymentEvent> msg = MessageBuilder
                .withPayload(event)
                .setHeader(PAYMENT_ID_HEADER, paymentId)
                .build();

        sm.sendEvent(msg);
    }

    /* build a statemachine via paymentId */
    private StateMachine<PaymentState, PaymentEvent> build(Long paymentId){
        Payment payment = repo.getOne(paymentId);
        StateMachine<PaymentState, PaymentEvent> sm = smFactory.getStateMachine(Long.toString(payment.getId()));

        sm.stop();

        sm.getStateMachineAccessor().doWithAllRegions(sma -> {

                sma.addStateMachineInterceptor(paymentStateChangeInterceptor);

                sma.resetStateMachine(new DefaultStateMachineContext<>(
                        payment.getState(),
                        null,
                        null,
                        null
                ));
        });

        sm.start();

        return sm;
    }
}
