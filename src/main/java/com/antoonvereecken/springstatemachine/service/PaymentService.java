package com.antoonvereecken.springstatemachine.service;

import com.antoonvereecken.springstatemachine.domain.Payment;
import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import org.springframework.statemachine.StateMachine;

public interface PaymentService {

    Payment newPayment(Payment payment);

    StateMachine<PaymentState, PaymentEvent> preAuthorizePayment(Long paymentId);
    StateMachine<PaymentState, PaymentEvent> authorizePayment(Long paymentId);
    StateMachine<PaymentState, PaymentEvent> declinePayment(Long paymentId);
}
