package com.antoonvereecken.springstatemachine.service.interceptor;


import com.antoonvereecken.springstatemachine.domain.Payment;
import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.repository.PaymentRepo;
import com.antoonvereecken.springstatemachine.service.PaymentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class PaymentStateChangeInterceptor extends StateMachineInterceptorAdapter<PaymentState, PaymentEvent> {

    private final PaymentRepo repo;

    /* before state change:
    *   - if msg is present -> get the paymentId off the header, set the state & persist to DB
    *
    * ~ handles the state of objects retrieved from DB
    **/
    @Override
    public void preStateChange(
            State<PaymentState, PaymentEvent> state,
            Message<PaymentEvent> message,
            Transition<PaymentState, PaymentEvent> transition,
            StateMachine<PaymentState, PaymentEvent> stateMachine
    ) {
        Optional.ofNullable(message).ifPresent(msg -> {
            Optional.ofNullable((Long) msg.getHeaders().getOrDefault(PaymentServiceImpl.PAYMENT_ID_HEADER, -1L))
                    .ifPresent(paymentId -> {
                        Payment p = repo.getOne(paymentId);
                        p.setState(state.getId());
                        repo.save(p);
                    });
        });
    }
}
