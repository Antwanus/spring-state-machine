package com.antoonvereecken.springstatemachine.service;

import com.antoonvereecken.springstatemachine.domain.Payment;
import com.antoonvereecken.springstatemachine.domain.PaymentEvent;
import com.antoonvereecken.springstatemachine.domain.PaymentState;
import com.antoonvereecken.springstatemachine.repository.PaymentRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.statemachine.StateMachine;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PaymentServiceImplTest {

    @Autowired PaymentService paymentService;
    @Autowired PaymentRepo repo;

    Payment p;

    @BeforeEach
    void setUp() {
        this.p = Payment.builder().amount(new BigDecimal("12.99")).build();
    }

    @Transactional
    @Test
    void preAuthorizePayment() {
        Payment savedPayment = paymentService.newPayment(this.p);

        System.out.println("Should be |> NEW <|");
        System.out.println(savedPayment.getState());
        StateMachine<PaymentState, PaymentEvent> sm =  paymentService.preAuthorizePayment(savedPayment.getId());

        Payment preAuthorizedPayment = repo.getOne(savedPayment.getId());


        System.out.println("Should be |> PRE_AUTH / PRE_AUTH_ERROR <|");
        System.out.println(sm.getState().getId());
        System.out.println(preAuthorizedPayment);
    }

    @Transactional
    @RepeatedTest(10)
    void authPayment(){
        Payment savedPayment = paymentService.newPayment(p);

        StateMachine<PaymentState, PaymentEvent> preAuthSM = paymentService.preAuthorizePayment(savedPayment.getId());

        if (preAuthSM.getState().getId() == PaymentState.PRE_AUTH) {
            System.out.println("+++++ PAYMENT IS PRE-AUTHORIZED +++++");

            StateMachine<PaymentState, PaymentEvent> authSM = paymentService.authorizePayment(savedPayment.getId());

            System.out.println("COMPLETED, CURRENT STATE = " + authSM.getState().getId());
        } else {
            System.out.println("----- PAYMENT FAILED PRE-AUTH!!!! -----");
        }
    }
}